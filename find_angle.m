clc;
clear all;
close all;

load('cameraParams')
load('cameraParamsBasic')


% Read the first image to obtain image size
originalImage = imread('C:\Users\khadi\Pictures\Camera Roll\3-meter.jpg');
% For example, you can use the calibration data to remove effects of lens distortion.
undistortedImage = undistortImage(originalImage, cameraParams);

imageArray = [originalImage undistortedImage];

cameraParams.IntrinsicMatrix
%montage(imageArray);

subplot(1,2,1), imshow(originalImage)
subplot(1,2,2), imshow(undistortedImage)

imwrite(undistortedImage,'C:\Users\khadi\Pictures\Camera Roll\3-meter-undistort.jpg')

